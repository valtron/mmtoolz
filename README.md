# mmtoolz

Multimedia tools powered by ffmpeg.

**Note: requires `ffmpeg` to be installed!**


## Sample Script

Check out the script [`sample/ps4.py`](/sample/ps4.py):

```sh
python sample/ps4.py SomeFile.mkv Output.mkv --debug
```

It re-encodes the given file to be playable on a PS4.


## Usage

Use the library in three phases:

### Read input files

Read the streams:

```python
import mmtoolz

# `input` returns `List[mmtoolz.CodedStream]`
cs1 = mmtoolz.input('file1.mp3')
cs2 = mmtoolz.input('video.mp4')
cs3 = mmtoolz.input('subs.srt')
```

and split them up however you want (e.g. video from this, audio from that):

```python
audio_cs = cs1[0]
video_cs = cs2[0]
audio_cs2 = cs2[1]
sub_cs = cs3[0]
```

### Do stuff with the streams

```python
from mmtoolz import ComputedStream

# Negate the video
video_neg = ComputedStream('negate', [video_cs.stream])
```

### Output them

Output two videos:

```python
mmtoolz.output({
	'vid1.mkv': [
		audio_cs,
		video_cs,
		sub_cs,
	],
	'vid2.mkv': [
		audio_cs2,
		CodedStream(video_neg, video_cs.codec),
		sub_cs,
		sub_cs, # Have two identical subtitle streams, why not!
	],
})
```


## Notes

Operations happen on `Stream`s, but `input` gives you `CodedStream`s
and `output` needs `CodedStream`s too. `CodedStream`s specify the encoding,
so they're only relevant for input/output.

The `ffmpeg` command created isn't optimized. Specifically, to avoid re-encoding
streams into the same codec, you should replace their `CodedStream.codec` with
a `'copy'` codec. See an example [here](/sample/ps4.py).
