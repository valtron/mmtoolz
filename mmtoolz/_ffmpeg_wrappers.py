from typing import Any, Dict
import json
import subprocess
from pathlib import Path

def ffprobe(filepath: Path) -> Any:
	out = _run_subprocess(
		'ffprobe', '-show_format', '-show_streams', '-of', 'json', filepath,
	)
	return json.loads(out.decode('utf-8'))

def ffmpeg(*args: Any, debug: bool = False) -> None:
	_ = _run_subprocess('ffmpeg', *args, '-y', debug = debug)

def _run_subprocess(*args: Any, debug: bool = False) -> bytes:
	run_args = [str(a) for a in args]
	
	kwargs: Dict[str, Any] = {}
	if not debug:
		kwargs = {
			'stdout': subprocess.PIPE,
			'stderr': subprocess.PIPE,
		}
	
	if debug:
		print("Running:")
		print(run_args)
	
	cp = subprocess.run(run_args, check = True, **kwargs)
	return cp.stdout
