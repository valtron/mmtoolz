from typing import Dict, Any, List, Union, Set, Iterable
import abc
from pathlib import Path

from . import _ffmpeg_wrappers

Pathlike = Union[str, Path]
CodecType = str

class Stream(abc.ABC):
	def __init__(self) -> None:
		pass

class CodedStream:
	def __init__(self, stream: Stream, codec: 'Codec') -> None:
		self.stream = stream
		self.codec = codec
	
	def to_codec(self, name: str) -> 'CodedStream':
		if self.codec.name == name:
			return self
		return CodedStream(self.stream, self.codec.to_codec(name))

class Codec:
	type: CodecType
	name: str
	
	@classmethod
	def FromJSON(cls, data: Dict[str, Any]) -> 'Codec':
		# codec, bitrate are part of codec
		# size, framerate are part of stream
		type = data['codec_type']
		name = data['codec_name']
		return cls(type, name)
	
	def __init__(self, type: CodecType, name: str) -> None:
		self.type = type
		self.name = name
	
	def to_codec(self, name: str) -> 'Codec':
		if self.name == name:
			return self
		return Codec(self.type, name)

def input(filepath: Pathlike) -> List[CodedStream]:
	filepath = _path(filepath)
	info = _ffmpeg_wrappers.ffprobe(filepath)
	return [
		CodedStream(InputStream(filepath, s), Codec.FromJSON(s))
		for s in info['streams']
	]

def output(outputs: Dict[Pathlike, List[CodedStream]], *, debug: bool = False) -> None:
	outputs = {
		_path(p): streams
		for p, streams in outputs.items()
	}
	
	g = _GraphBuilder()
	
	output_opts: List[Any] = []
	
	for p, streams in sorted(outputs.items()):
		for s in streams:
			name = g.get_output_name(s.stream)
			output_opts.extend([
				'-map',
				('[{}]'.format(name) if name.startswith('a') else name),
			])
		for i, s in enumerate(streams):
			output_opts.extend([
				'-c:{}'.format(i), s.codec.name,
			])
		output_opts.append(p)
	
	input_opts: List[Any] = []
	for p in g.input_paths:
		input_opts.append('-i')
		input_opts.append(p)
	
	filter_opts: List[Any]
	filter_graph = g.get_graph_text()
	if filter_graph:
		filter_opts = ['-filter_complex', filter_graph]
	else:
		filter_opts = []
	
	return _ffmpeg_wrappers.ffmpeg(
		*input_opts,
		*filter_opts,
		*output_opts,
		debug = debug,
	)

class ComputedStream(Stream):
	def __init__(self, filter: str, inputs: List[Stream], arg: Any = None, **kwargs: Any) -> None:
		assert (arg is not None) != bool(kwargs)
		
		self.filter = filter
		self.inputs = inputs
		self.arg = arg
		self.kwargs = kwargs

class _GraphBuilder:
	input_paths: List[Path]
	_output_names: Dict[ComputedStream, str]
	_name_index: int
	
	def __init__(self) -> None:
		self.input_paths = []
		self._output_names = {}
		self._name_index = 0
	
	def get_graph_text(self) -> str:
		ops: List[ComputedStream] = []
		collected: Set[ComputedStream] = set()
		for s in self._output_names.keys():
			ops.extend(self._collect_graph_ops(s, collected))
		
		op_strs: List[str] = []
		for cs in ops:
			op_str = ''
			for si in cs.inputs:
				op_str += '[{}]'.format(self.get_output_name(si))
			op_str += cs.filter
			if cs.arg is not None:
				op_str += '={}'.format(cs.arg)
			elif cs.kwargs:
				op_str += '='
				op_str += ':'.join('{}={}'.format(k, v) for k, v in cs.kwargs.items())
			op_str += '[{}]'.format(self.get_output_name(cs))
		
		return ';'.join(op_strs)
	
	def _collect_graph_ops(self, stream: ComputedStream, collected: Set[ComputedStream]) -> Iterable[ComputedStream]:
		if stream in collected:
			return
		collected.add(stream)
		for s in stream.inputs:
			if not isinstance(s, ComputedStream):
				continue
			yield from self._collect_graph_ops(s, collected)
		yield stream
	
	def get_output_name(self, stream: Stream) -> str:
		if isinstance(stream, InputStream):
			file_index = self._get_input_file_index(stream.filepath)
			return '{}:{}'.format(file_index, stream.index)
		assert isinstance(stream, ComputedStream)
		if stream not in self._output_names:
			self._output_names[stream] = self._get_next_name()
		return self._output_names[stream]
	
	def _get_input_file_index(self, filepath: Path) -> int:
		if filepath in self.input_paths:
			return self.input_paths.index(filepath)
		idx = len(self.input_paths)
		self.input_paths.append(filepath)
		return idx
	
	def _get_next_name(self) -> str:
		i = self._name_index
		self._name_index += 1
		return 'a{}'.format(i)

class InputStream(Stream):
	def __init__(self, filepath: Path, info: Dict[str, Any]) -> None:
		self.filepath = filepath
		self.index = info['index']
		self._info = info

def _path(pathlike: Pathlike) -> Path:
	if not isinstance(pathlike, Path):
		pathlike = Path(pathlike)
	return pathlike
