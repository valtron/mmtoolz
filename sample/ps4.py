from typing import List
from pathlib import Path
from collections import defaultdict
import mmtoolz
from mmtoolz import CodedStream

def main(infile: Path, outfile: Path, *, debug: bool = False) -> None:
	streams = mmtoolz.input(infile)
	streams = _convert(streams)
	mmtoolz.output({
		outfile: streams,
	}, debug = debug)

def _convert(streams: List[CodedStream]) -> List[CodedStream]:
	out_streams = []
	
	by_type = defaultdict(list)
	for s in streams:
		by_type[s.codec.type].append(s)
	
	if by_type['video']:
		sv = by_type['video'][0]
		sv = _copy_hack(sv, 'h264')
		out_streams.append(sv)
	
	if by_type['audio']:
		sa = by_type['audio'][0]
		sa = _copy_hack(sa, 'aac')
		out_streams.append(sa)
	
	if by_type['subtitle']:
		ss = by_type['subtitle'][0]
		ss = _copy_hack(ss, 'ass')
		out_streams.append(ss)
	
	return out_streams

def _copy_hack(cs: CodedStream, codec: str) -> CodedStream:
	# TODO: optimize to auto-insert `copy` codecs in the graph builder
	if cs.codec.name == codec:
		return cs.to_codec('copy')
	return cs.to_codec(codec)

if __name__ == '__main__':
	import funcli
	funcli.main()
