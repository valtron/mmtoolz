import setuptools

with open('README.md', 'r') as fh:
	long_description = fh.read()

setuptools.setup(
	name = 'mmtoolz',
	version = '0.1.0',
	author = "valtron",
	description = "Multimedia tools powered by ffmpeg.",
	long_description = long_description,
	long_description_content_type = 'text/markdown',
	url = 'https://gitlab.com/valtron/mmtoolz',
	py_modules = ['mmtoolz'],
	python_requires = '>=3.6',
	classifiers = [
		'Programming Language :: Python :: 3 :: Only',
		'Programming Language :: Python :: 3.6',
		'Programming Language :: Python :: 3.7',
		'Programming Language :: Python :: 3.8',
		'Programming Language :: Python :: 3.9',
		'Operating System :: OS Independent',
		'License :: OSI Approved :: MIT License',
	],
)
